export enum LOCALES {
  UA = "ua",
  EN = "en",
  RU = "ru",
}
