import { LOCALES } from "./locales.ts";
type Values = {
  value: string;
  locales: LOCALES[];
};
export const MESSAGES: Record<string, { default: string; values: Values[] }> = {
  WELDER_WORK: {
    default: "Welder work",
    values: [
      { value: "Welder work", locales: [LOCALES.EN] },
      { value: "Сварочные работы", locales: [LOCALES.RU] },
    ],
  },
  NAVBAR_MAIN: {
    default: "Welder work",
    values: [
      { value: "Welder work", locales: [LOCALES.EN] },
      { value: "Сварочные работы", locales: [LOCALES.RU] },
    ],
  },
  NAVBAR_ABOUT: {
    default: "Welder work",
    values: [
      { value: "Welder work", locales: [LOCALES.EN] },
      { value: "Сварочные работы", locales: [LOCALES.RU] },
    ],
  },
  NAVBAR_SERVICE: {
    default: "Welder work",
    values: [
      { value: "Welder work", locales: [LOCALES.EN] },
      { value: "Сварочные работы", locales: [LOCALES.RU] },
    ],
  },
  NAVBAR_PRICE: {
    default: "Welder work",
    values: [
      { value: "Welder work", locales: [LOCALES.EN] },
      { value: "Сварочные работы", locales: [LOCALES.RU] },
    ],
  },
  NAVBAR_PORTFOLIO: {
    default: "Welder work",
    values: [
      { value: "Welder work", locales: [LOCALES.EN] },
      { value: "Сварочные работы", locales: [LOCALES.RU] },
    ],
  },
  NAVBAR_ARTICLES: {
    default: "Welder work",
    values: [
      { value: "Welder work", locales: [LOCALES.EN] },
      { value: "Сварочные работы", locales: [LOCALES.RU] },
    ],
  },
  NAVBAR_CONTACTS: {
    default: "Welder work",
    values: [
      { value: "Welder work", locales: [LOCALES.EN] },
      { value: "Сварочные работы", locales: [LOCALES.RU] },
    ],
  },
  NAVBAR_ORDER_CALCULATION: {
    default: "Welder work",
    values: [
      { value: "Welder work", locales: [LOCALES.EN] },
      { value: "Сварочные работы", locales: [LOCALES.RU] },
    ],
  },
};
