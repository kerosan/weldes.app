import { createIntl, createIntlCache } from "@formatjs/intl";

// This is optional but highly recommended
// since it prevents memory leak
const cache = createIntlCache();

export const intl = createIntl({
  locale: "ru", //LOCALES.RU,
  messages: {
    WELDER_WORK: "Сварочные работы",
  },
  onError: (error: any) => {
    console.log("IntlError", error);
  },
}, cache);
