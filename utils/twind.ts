import { IS_BROWSER } from "$fresh/runtime.ts";
import { Configuration, setup } from "twind";
export * from "twind";
import * as colors from "twind/colors";

export const theme = {
  colors: {
    orange: {
      ...colors.orange,
      450: "#FA983E",
      451: "#FA9C46",
      452: "#FBA85D",
    },
    blue: colors.blue,
    black: colors.black,
    gray: { ...colors.gray, 350: "#b1b1b1" },
    green: colors.green,
    white: colors.white,
    yellow: colors.yellow,
    transparent: "transparent",
  },
};
export const config: Configuration = {
  darkMode: "class",
  mode: "silent",
  theme,
};
if (IS_BROWSER) setup(config);
