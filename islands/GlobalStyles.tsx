/** @jsx h */
import { h } from "preact";
import { tw } from "@twind";
import { css } from "twind/css";

const globalStyles = css({
  ":global": {
    body: {
      fontFamily: "Open Sans, sans-serif",
    },
  },
});

export default function GlobalStyles() {
  return (
    <div
      class={tw(globalStyles)}
    />
  );
}
