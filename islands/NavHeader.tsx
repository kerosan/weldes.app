/** @jsx h */
/** @jsxFrag Fragment */
import { Fragment, h } from "preact";
import { apply, tw } from "@twind";
import { asset } from "$fresh/runtime.ts";

import Button from "~/islands/Button.tsx";
import { intl } from "~/utils/i18n/intl.ts";

const logo = asset("/assets/vector/logo.svg");

const menuItemCss = apply(`text-white px-4 target:(border-b-4)`);

export default function NavHeader() {
  return (
    <div class={tw`sticky top-0`}>
      <header class={tw`h-[88px] bg-white bg-opacity-[0.15]`}>
        <div class={tw`flex w-[1152px] m-auto mw-max-content`}>
          <div class={tw`flex py-5 w-[220px]`}>
            <img src={logo} alt="logo of weldes" />
            <div class={tw`flex flex-col pl-3 text-center text-white`}>
              <span class={tw`text-4xl font-extrabold`}>WELDES</span>
              <span class={tw`text-xs uppercase`}>
                {intl.formatMessage({ id: "WELDER_WORK" })}
              </span>
            </div>
            <div>
            </div>
          </div>
        </div>
      </header>

      <header class={tw`block m-0 p-0 h-[68px]  bg-white bg-opacity-[0.15]`}>
        <div
          class={tw
            `flex w-[1152px] m-auto mw-max-content flex-row items-center gap-2 justify-between`}
        >
          <menu class={tw`flex list-none`}>
            <li class={tw(menuItemCss)}>
              <a href="/">
                Главная
              </a>
            </li>
            <li class={tw(menuItemCss)}>
              <a href="/">
                О нас
              </a>
            </li>
            <li class={tw(menuItemCss)}>
              <a href="/">Услуги</a>
            </li>
            <li class={tw(menuItemCss)}>
              <a href="/">Цены</a>
            </li>
            <li class={tw(menuItemCss)}>
              <a href="/">Наши работы</a>
            </li>
            <li class={tw(menuItemCss)}>
              <a href="/">Статьи</a>
            </li>
            <li class={tw(menuItemCss)}>
              <a href="/">Контакты</a>
            </li>
          </menu>
          <Button>Заказать просчёт</Button>
        </div>
      </header>
    </div>
  );
}
