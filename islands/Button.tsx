/** @jsx h */
import { FunctionalComponent, h } from "preact";
import { tw } from "@twind";

const Button: FunctionalComponent<h.JSX.IntrinsicElements["button"]> = (
  props,
) => {
  return (
    <button
      {...props}
      class={tw`text-white
        bg-orange-450
        rounded-[3px]
        min-w-[80px] min-h-[36px] px-4
        uppercase whitespace-nowrap
        hover:(bg-orange-451 rounded-[2px])
        outline-none
        active:(bg-orange-452 rounded-[2px])
        focus:(outline-none)
        disabled:bg-gray-350
        `}
    />
  );
};

export default Button;
