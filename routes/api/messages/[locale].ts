import { HandlerContext } from "$fresh/server.ts";

import { MESSAGES } from "~/utils/i18n/messages.ts";
import { LOCALES } from "~/utils/i18n/locales.ts";

export const handler = (_req: Request, _ctx: HandlerContext): Response => {
  const { locale } = _ctx.params;
  const mapFn = (key: string) => [
    key,
    MESSAGES[key as keyof typeof MESSAGES].values.find((value) =>
      value.locales.includes(locale as LOCALES)
    )?.value || MESSAGES[key as keyof typeof MESSAGES].default,
  ];
  const filteredMessages = Object.fromEntries(Object.keys(MESSAGES).map(mapFn));

  return new Response(JSON.stringify(filteredMessages), {
    headers: { "Content-Type": "application/json" },
  });
};
