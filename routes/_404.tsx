/** @jsx h */
import { FunctionComponent, h } from "preact";
import { PageProps } from "$fresh/server.ts";

const NotFound: FunctionComponent<PageProps> = (props) => {
  return <div>not found</div>;
};
export default NotFound;
