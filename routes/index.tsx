/** @jsx h */
/** @jsxFrag Fragment */
import { Fragment, h } from "preact";
import { tw } from "@twind";

import { asset } from "$fresh/runtime.ts";
import HtmlHead from "../islands/HtmlHead.tsx";
import GlobalStyles from "../islands/GlobalStyles.tsx";
import NavHeader from "../islands/NavHeader.tsx";

const screen1 = asset("/assets/navMenu/main.png");

export default function Index() {
  return (
    <>
      <HtmlHead />
      <GlobalStyles />
      <main class={tw`flex flex-col min-h-screen`}>
        <div
          class={tw`bg-cover h-[848px]`}
          style={{ backgroundImage: `url(${screen1})` }}
        >
          <NavHeader />
        </div>
      </main>
    </>
  );
}
